import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def read_data(number):
    DATA_PATH = "Lab1\\Data\\Data"
    return conver_data(pd.read_excel(DATA_PATH + str(number) + ".xlsx"))


def conver_data(data):
    return data.to_numpy()


def initial_moment(data, r):
    moment = (1 / (len(data))) * np.sum(data ** r)
    return moment


def central_moment(data, r, math_exp):
    moment = (1 / (len(data) - 1)) * np.sum((data - math_exp) ** r)
    return moment


def standardization_matrix(database):
    data, math_expectations, standard_deviations = database
    u = np.array([(value - math_expectations[index]) / standard_deviations[index] for index, value in enumerate(data)])
    return u.transpose()


def average_error(database, index):
    data, math_expectations, dispersions,standard_deviations = database
    x_index, y_index = index
    m = dispersions[y_index] * (
        1 / len(data[x_index]) + (np.power(data[x_index] - math_expectations[x_index], 2) / (
            np.sum(np.power(data[x_index] - math_expectations[x_index], 2)))))
    r = 1 / (len(data[x_index]) - 2) * np.sum(np.power(data[y_index] - math_expectations[y_index], 2))
    error = np.sqrt(m + r)
    return error


if __name__ == '__main__':
    k = 1
    data = read_data(k).transpose()
    if k == 2:
        data = np.delete(data, [0], axis=0)

    math_expectations = [initial_moment(x, 1) for x in data]
    dispersions = [central_moment(value, 2, math_expectations[index]) for index, value in enumerate(data)]
    standard_deviations = [x ** 0.5 for x in dispersions]

    math_expectations = np.array(math_expectations)
    dispersions = np.array(dispersions)
    standard_deviations = np.array(standard_deviations)

    print('math_expectations -->', math_expectations)
    print('dispersions -->', dispersions)
    print('standard_deviations -->', standard_deviations)

    u = standardization_matrix((data, math_expectations, standard_deviations))
    u = u.transpose()

    correlation_coefficients = np.array([np.sum(x * u[0]) * (1 / len(x)) for x in u[1:]])
    print('correlation_coefficients -->', correlation_coefficients)
    print(u)
    n = 15
    student_t = ((n - 2) ** 0.5 / ((1 - correlation_coefficients ** 2) ** 0.5)) * abs(correlation_coefficients)
    print('student_t -->', student_t, end='\n\n')
    flag = True
    x_index = 0
    y_index = 1
    # if flag:
    #     plt.plot(data[y_index], data[x_index], 'o')
    #     plt.grid(True)
    #     plt.axis([min(data[y_index]) * 0.8, max(data[y_index]) * 1.1, min(data[x_index]) * 0.8, max(data[x_index]) * 1.1])
    #     plt.show()
    a = math_expectations[x_index] - correlation_coefficients[x_index] * math_expectations[y_index] * (
            standard_deviations[x_index] / standard_deviations[y_index])
    b = correlation_coefficients[x_index] * (
            standard_deviations[x_index] / standard_deviations[y_index])
    y_approximate = lambda x:  a + b * x
    if flag:
        t = np.arange(32, 55, 2)
        if k == 2:
            t = np.arange(3500, 4000, 50)
        plt.plot(t,y_approximate(t))
        plt.scatter(data[y_index],data[x_index])
        plt.show()
    #error = average_error((data, math_expectations, dispersions,standard_deviations), (0, 1))
    #print('average_error -->', error)
    print('Error -->', data[x_index] - y_approximate(data[y_index]))
