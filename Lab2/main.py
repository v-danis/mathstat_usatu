import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == '__main__':
    m = 48
    a = 1/(2*m+1)
    y = []
    data = pd.read_csv("Lab2/Data/monthly-beer-production-in-austr.csv", sep=',')
    for t in range(m,len(data)-m):
        x_y = 0
        for k in range(-m,m+1):
            x_y += a*data['Monthly beer production'][t+k]
        y.append([data['Month'][t],x_y])

    print(y)
    y = pd.DataFrame(y)
    data = data[m:-m]
    y = y.rename(columns={0:'Month', 1:'Window method'})

    data['Month'] = pd.to_datetime(data['Month'])
    print(y)
    y['Month'] = pd.to_datetime(y['Month'])

    data = data.set_index('Month')
    y = y.set_index('Month')

    data = data['Monthly beer production']
    y = y['Window method']
    ax = y.plot()
    data.plot(ax=ax,figsize=(14,7))
    plt.show()
#
# import warnings
# import itertools
# import pandas as pd
# import numpy as np
# import statsmodels.api as sm
# import matplotlib.pyplot as plt
# plt.style.use('fivethirtyeight')
#
# def load_data():
#     data = sm.datasets.co2.load_pandas()
#     y = data.data
#     y = y['co2'].resample('MS').mean()
#     y = y.fillna(y.bfill())
#     return y
#
# if __name__ == '__main__':
#     y = load_data()
#     # y.plot(figsize=(15, 6))
#     # plt.show()
#
#     # Определите p, d и q в диапазоне 0-2
#     p = d = q = range(0, 2)
#     # Сгенерируйте различные комбинации p, q и q
#     pdq = list(itertools.product(p, d, q))
#     # Сгенерируйте комбинации сезонных параметров p, q и q
#     seasonal_pdq = [(x[0], x[1], x[2], 12) for x in pdq]
#
#     warnings.filterwarnings("ignore") # отключает предупреждения
#     for param in pdq:
#         for param_seasonal in seasonal_pdq:
#             try:
#                 mod = sm.tsa.statespace.SARIMAX(y,
#                 order=param,
#                 seasonal_order=param_seasonal,
#                 enforce_stationarity=False,
#                 enforce_invertibility=False)
#                 results = mod.fit()
#                 print('ARIMA{}x{}12 - AIC:{}'.format(param, param_seasonal, results.aic))
#             except:
#                 continue
#     mod = sm.tsa.statespace.SARIMAX(y,
#                                     order=(1, 1, 1),
#                                     seasonal_order=(1, 1, 1, 12),
#                                     enforce_stationarity=False,
#                                     enforce_invertibility=False)
#     results = mod.fit()
#     print(results.summary().tables[1])
#     results.plot_diagnostics(figsize=(15, 12))
#     plt.show()
#
#     pred = results.get_prediction(start=pd.to_datetime('1998-01-01'), dynamic=False)
#     pred_ci = pred.conf_int()
#
#     ax = y['1990':].plot(label='observed')
#     pred.predicted_mean.plot(ax=ax, label='One-step ahead Forecast', alpha=.7)
#     ax.fill_between(pred_ci.index,
#                     pred_ci.iloc[:, 0],
#                     pred_ci.iloc[:, 1], color='k', alpha=.2)
#     ax.set_xlabel('Date')
#     ax.set_ylabel('CO2 Levels')
#     plt.legend()
#     plt.show()
#
#     y_forecasted = pred.predicted_mean
#     y_truth = y['1998-01-01':]  # Compute the mean square error
#     mse = ((y_forecasted - y_truth) ** 2).mean()
#     print('The Mean Squared Error of our forecasts is {}'.format(round(mse, 2)))
#
#     pred_dynamic = results.get_prediction(start=pd.to_datetime('1998-01-01'), dynamic=True, full_results=True)
#     pred_dynamic_ci = pred_dynamic.conf_int()
#
#     ax = y['1990':].plot(label='observed', figsize=(20, 15))
#     pred_dynamic.predicted_mean.plot(label='Dynamic Forecast', ax=ax)
#     ax.fill_between(pred_dynamic_ci.index,
#                     pred_dynamic_ci.iloc[:, 0],
#                     pred_dynamic_ci.iloc[:, 1], color='k', alpha=.25)
#     ax.fill_betweenx(ax.get_ylim(), pd.to_datetime('1998-01-01'), y.index[-1],
#                      alpha=.1, zorder=-1)
#     ax.set_xlabel('Date')
#     ax.set_ylabel('CO2 Levels')
#     plt.legend()
#     plt.show()
#     # Извлечь прогнозируемые и истинные значения временного ряда
#     y_forecasted = pred_dynamic.predicted_mean
#     y_truth = y['1998-01-01':]  # Вычислить среднеквадратичную ошибку
#     mse = ((y_forecasted - y_truth) ** 2).mean()
#     print('The Mean Squared Error of our forecasts is {}'.format(round(mse, 2)))
#
#     # Получить прогноз на 500 шагов вперёд
#     pred_uc = results.get_forecast(steps=500)
#     # Получить интервал прогноза
#     pred_ci = pred_uc.conf_int()
#
#     ax = y.plot(label='observed', figsize=(20, 15))
#     pred_uc.predicted_mean.plot(ax=ax, label='Forecast')
#     ax.fill_between(pred_ci.index,
#                     pred_ci.iloc[:, 0],
#                     pred_ci.iloc[:, 1], color='k', alpha=.25)
#     ax.set_xlabel('Date')
#     ax.set_ylabel('CO2 Levels')
#     plt.legend()
#     plt.show()